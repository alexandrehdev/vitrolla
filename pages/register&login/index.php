<?php 
// require_once("../../Classes/Controller/GetRegisterData.php");
require_once("../../autoload.php");

if (isset($_POST['register-btn'])) {
	(new GetRegisterData());
	(new UserRegisterController())->registerUser();
}

// if (isset($_POST['login-btn'])) {
// 	(new UserLoginController())->sendModel();
// }
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Register</title>
	<link rel="stylesheet" type="text/css" href="../../public/register&login/css/style.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/icon.min.css">
</head>
<body>
	<header class="hl-header">
		<h1>
			Vitrolla
		</h1>
		<div class="hl-menu">
			<div>
				<a href="../../index.php" name="register">
					<i class="long arrow alternate left icon" style="font-size: 40px;"></i>
				</a>
			</div>
		</div>
	</header>
	<main>
		<div class="sideRegister">
			<form method="POST" action="<?php $_SERVER['PHP_SELF']?>">
				<h2 style="text-align: center; color:white;">Register</h2>
				<div>
					<i class="user icon" style="color: white;"></i>
					<input type="text" name="name" autocomplete="off" placeholder="Username">
				</div>
				<div>
					<i class="mail icon" style="color: white;"></i>
					<input type="text" name="email" autocomplete="off" placeholder="Email">
				</div>
				<div>
					<i class="key icon" style="color: white;"></i>
					<input type="password" name="pwd" autocomplete="off" placeholder="Password">
				</div>
				<div>
					<i class="key icon" style="color: white;"></i>
					<input type="password" name="cpwd" autocomplete="off" placeholder="Confirm Password">
				</div>
				<div class="registerBtn">
					<button name="register-btn">
						<i class="pencil alternate icon"></i>
					</button>
				</div>
			</form>
		</div>

		<div class="sideLogin">
			<form method="POST" action="<?php $_SERVER['PHP_SELF']?>">
				<h2 style="text-align:center; color: white;">Login</h2>
				<div>
					<i class="mail icon" style="color: white;"></i>
					<input type="text" name="email_login" autocomplete="off" placeholder="Email">
				</div>
				<div>
					<i class="key icon" style="color: white;"></i>
					<input type="password" name="pwd_login" autocomplete="off" placeholder="Password">
				</div>
				<div class="loginBtn">
					<button name="login-btn">
						<i class="paper plane icon"></i>
					</button>
				</div>
			</form>
		</div>
	</main>
	<script type="text/javascript" src="../../public/register&login/js/script.js"></script>
</body>
</html>