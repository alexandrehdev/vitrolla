<?php 
	
	class GetRegisterData
	{
		private $username;
		private $email;
		private $password;
		private $cpassword;

		function __construct()
		{
			$this->setUsername($_POST['name']);
			$this->setEmail($_POST['email']);
			$this->setPassword($_POST['pwd']);
			$this->setCPassword($_POST['cpwd']);
		}

		public function getUsername(){
			return $this->username;
		}

		public function setUsername($user){
			$this->username = $user;
		}

		public function getEmail(){
			return $this->email;
		}

		public function setEmail($mail){
			$this->email = $mail;
		}

		public function getPassword(){
			return $this->password;
		}

		public function setPassword($pass){
			$this->password = $pass;
		}

		public function getCPassword(){
			return $this->cpassword;
		}

		public function setCPassword($cpass){
			$this->cpassword = $cpass;
		}
	}

 ?>