<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="public/mainpage/css/style.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/icon.min.css">

	<title>Vitrolla</title>
</head>
<body>
	<header class="hl-header">
		<h1 class="header-title">
			Vitrolla 
		</h1>
		<div class="hl-menu">
			<div>
				<a href="pages/register&login/index.php" class="start-button" name="register">
					Começar
				</a>
			</div>
		</div>
	</header>
	<main>
		<div class="info-block">
			<h1>Crie sua playlists</h1>
			<span>Compartilhe seu gosto musical</span>
			<br>
			<br>
			<span>
				<i class="headphones icon"></i>
			</span>
		</div>

		<div class="info-block mygrey">
			<h1>Descubra sobre seu artista</h1>
			<span>Saiba informações sobre futuros shows</span>
			<br>
			<br>
			<span>
				<i class="globe icon"></i>
			</span>

		</div>

		<div class="info-block myblack">
			<h1>Faça amigos</h1>
			<span>Converse com outros usuários</span>
			<br>
			<br>
			<span>
				<i class="comments icon"></i>
			</span>
		</div>

	</main>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
	<script type="text/javascript" src="public/mainpage/js/script.js"></script>
	<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>
</body>
</html>
